#include "set_func.h"
#include <string.h>
/*fflusch.c */


//Aufgabe 1.
double ask4DoubleNonneg(const char* verwendungszweck) {
	double zahl_d;
	do {
		//printf
		printf_s("%s", verwendungszweck);
		scanf_s("%lf", &zahl_d);
		if (zahl_d<0)
		{
			printf_s("Ihre Zahl ist negativ, geben Sie eine positive Zahl ein \n");
		}
		CleanUp();
	}
	while (zahl_d < 0);
	return zahl_d;
	
}
//Aufgabe 2.
int askIntInBounds(const char*verwendungszweck, int min, int max) {
	
	int numb;
	//Um eine Widersp�chliche Parameterkombination zu vermeiden, sollen wir erstmal pr�fen, ob Minimum< Maximum ist
	//Falls die Kombination nicht stimmt, gibt die Funktion ein 0 zur�ck.
	
	if (min >= max)
	{
		printf(" Falsche Parameternutzung, ein Minimum soll kleiner als Maximum sein!");
		return 0;
	}
	printf("%s", verwendungszweck);
	
		do 
		{
			scanf_s("%d", &numb);
			if (numb<min||numb>max)
			{
			printf_s("Geben Sie die Zahl, die sich innerhalb des[%d,%d] Intervalls befindet \n", min, max);
			}

			CleanUp();
		} while (numb<min || numb> max);
		
		
	return numb;


}



//Aubgabe 3.
double ask4DoubleInBounds(char*verwendungszweck3, double min, double max){
	//Die zwei letzte Parameter sollen true sein, das bedeutet abgeschlossenes Intervall, siehe Aufgabe 4

	double numb=askDoubleWithConstraints(verwendungszweck3, min, max, true, true);

	return numb;
}
 

//Aufgabe 4
double askDoubleWithConstraints(const char* verwendungszweck, double min, double max, bool bound1, bool bound2) {
	//Um eine Widersp�chliche Parameterkombination zu vermeiden, sollen wir erstmal pr�fen, ob Minimum< Maximum ist
	if (min >= max)
	{
		printf(" Falsche Parameternutzung, ein Minimum soll kleiner als Maximum sein!");
		return 0;
	}

	printf("%s", verwendungszweck);
	double numb;
	int count;
	do {
		count = 0;
		scanf_s("%lf", &numb);
		//Die beide logische Werte bound1 und bound2 representieren jeweils das abgeschlossene und offene Intervall
		//true bedeutet abgeschlossenes Intervall, false offenes
		//Bound1 representiert Minimumwert von links, bound2 Maximumwert von rechts

		if (bound1 && numb >= min || !bound1 && numb > min) {
			count++;
		}
		if (bound2  && numb <= max || !bound2 && numb < max) {
			count++;
		}
		if (count == 2)
		{
			return numb;
		}
		
		printf_s("Ihre Zahl befindet sich ausserhlb des Intervalls %lf und %lf, wiederholen Sie die EIngabe \n", min, max);
		CleanUp();
		
	} while (count != 2);
}


// Aufgabe 5

int chooseFromMenu(const char* verwendungszweck5, char** array, int anzahl) {
	//Der Index wird in der Function ausgegeben
	int index;
	char check[5];
	memset(check, 0, 5 * sizeof(char));
	int compare = 1;
	

	printf("%s \n", verwendungszweck5);
	for (int i = 0, index = 1; i < anzahl, index <= anzahl; i++, index++)
	{
		printf("%d%c%s \n", index, '.', array[i]);
	}

	do { //until user didn't confirm his choice

		do {//until user didn't choose existing index
			index = askIntInBounds("", 1, anzahl);
		} while (index <1 || index>anzahl);

		//Pointer to the chosen product 
		char *pointer = array[index - 1];
		printf_s(" Sind Sie sicher, dass Sie %s auswahlen  wollen? \n Tippen Sie 'ja' zur Bestaetigung und 'nein' zum Abbruch \n", pointer);
		
		scanf_s("%s", check,4);

		for (int i = 0; i <strlen(check); i++) {
			
			check[i] = toupper(check[i]);
		}
		
		compare = strcmp(check, "JA");
		CleanUp();
		if (compare == 0) {
			return index;

		}
		else{
			printf("Wiederholen Sie den Auswahl \n");
		}
	} while (compare != 0);

}
//Zusatz
void CleanUp(){
	scanf_s("%*[^\n]");
}