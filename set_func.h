
#ifndef HEADER_H
#include <stdbool.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#define HEADER_H
void CleanUp();
double ask4DoubleNonneg(const char* verwendungszweck);
int askIntInBounds(const char* verwendungszweck2, int min, int max);
double ask4DoubleInBounds(char*verwendungszweck3, double min3, double max3);
double askDoubleWithConstraints(const char* verwendungszweck, double min, double max, bool bound1, bool bound2 );
int chooseFromMenu(const char* verwendungszweck5, char** array, int anzahl);
#endif